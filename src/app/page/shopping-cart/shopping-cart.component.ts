import { Component, OnInit } from '@angular/core';

/** Services */
import { WebSqlService } from '../../core/services/websql.service'

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  params    : Array<any>  = [];
  dataTable : Array<any>  = [];
  total     : number  = 0; 

  constructor(
    private _WebSql   : WebSqlService
  ) { }

  ngOnInit() {

    let queryt = "SELECT id_product, name, value, quantity, (quantity*value) AS total FROM products"
    this._WebSql.QueryData(queryt).then((response) => {
      if(response == false){
        this.dataTable  = [];
      }else{
        this.dataTable = response;          
      }
    });
     
    let query = "SELECT SUM(quantity*value) AS total FROM products"
    this._WebSql.QueryData(query, false).then((response) => {
      this.total = response.total;
    });


  }

  deleteProduct(id_product){

    this.params['table']      = "products";
    this.params['condition']  = "id_product = '" + id_product +"'";

    this._WebSql.DeleteData(this.params).then((response) => {
      this.ngOnInit();
    });

  }

  SaveOrder(){

    this.params['table']      = "products";
    this.params['condition']  = "";

    this._WebSql.DeleteData(this.params).then((response) => {
      this.ngOnInit();
    });

  }

}
