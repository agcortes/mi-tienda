import { Component, DoCheck} from '@angular/core';
import { first } from 'rxjs/operators';

/** Pipes **/
import { OrderPipe } from 'ngx-order-pipe';

/** Services **/
import { ProductsService } from '../../core/services/products.service'
import { WebSqlService } from '../../core/services/websql.service'

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements DoCheck {
  
  products        : any[]       = [];
  totalproductos  : any[]       = [];
  params          : Array<any>  = [];
  data            : any         = {};
  id_category     : number      = 0;
  sublevels       : boolean = false;
 
  public ordenar  : Array<{value: string, label: string}> = [{value : 'quantity', label : 'Cantidad'}, {value : 'available', label : 'Disponibilidad'}, {value : 'price', label : 'Precio'}];
  
  public position  : Array<{value: string, label: string}> = [{value : '', label : 'Ascendente'}, {value : 'false', label : 'Descendente'}];
  
  public listdisponibilidad  : Array<{value: number, label: string}> = [{value : 1, label : 'Disponible'}, {value : 0, label : 'No Disponible'}];

  order       : string  = '';
  reverse     : boolean = false;
  search      : string  = '';  
  disponible  : string  = ''
  readp       : boolean = false;
  visible     : boolean = false; 

  constructor(    
    private _Products : ProductsService,
    private _WebSql   : WebSqlService,
  ) { }

  ngOnInit() {}

  ngDoCheck(){

    if(this.search.length >= 2){

      this.readp    = true;
      this.visible  = true;

      if(this.disponible != ''){
                
        this.totalproductos = this.products.filter(datos =>  
          datos.name.toLowerCase().indexOf(this.search.toLowerCase()) > -1 && datos.sublevel_id == this.id_category && datos.available == this.disponible
        );
              
      }else{
        
        this.totalproductos = this.products.filter(datos =>  
          datos.name.toLowerCase().indexOf(this.search.toLowerCase()) > -1 && datos.sublevel_id == this.id_category
        );
      }      
      
    }else{
      
      if(this.readp == true){
        this.totalproductos = this.products.filter(datos =>
          datos.sublevel_id == this.id_category
        );
        this.readp = false;        
      }
     
    }
    
  }

  public publicproducts(id_category : number = 0, sublevels = true){
    
    this.visible        = false;
    this.totalproductos = [];
    this.id_category    = id_category;

    this.sublevels      = sublevels;   
    
    if(id_category > 0){
    
      this._Products.getProducts().pipe(first())
      .subscribe(
        data => {
          
          this.products = data.products; 
          
          this.totalproductos = this.products.filter(datos =>
            datos.sublevel_id == this.id_category
          );
      
      });       

    }
  
  }

  setCar(data: any){

    this.params['table']      = "products";
    this.params['condition']  = "id_product = '" + data.id +"'";

    this._WebSql.ReadData(this.params).then((response) => {

      if(response == false){

        this.params['campos'] = "id_product, quantity, name, value";
        
        this.params['value']  = [];
        this.params['value'].push(data.id);
        this.params['value'].push(1);
        this.params['value'].push(data.name);
        this.params['value'].push(data.price);

        this._WebSql.InsertData(this.params).then((response) => {});

      }else{  

        debugger;
        this.params['campos']  = "quantity";

        this.params['value']  = [];
        this.params['value'].push(1);

        this._WebSql.UpdateData(this.params).then((response) => {});
      
      }
        
    });

  }

  filtrar(){

    if(this.disponible != ''){
      this.totalproductos = this.products.filter(datos =>  
        datos.available == this.disponible && datos.sublevel_id == this.id_category
      );
    }

  } 
  
}
