import { Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { first, find } from 'rxjs/operators';

/** Services **/
import { CategoriesService } from '../../core/services/categories.service';

/** Components */
import { ProductsComponent } from '../products/products.component';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})

export class CategoriesComponent implements OnInit {

  @ViewChild(ProductsComponent) Products : ProductsComponent;

  public name     : string;
  public find     : number = 0;
  public fin      : boolean = false;
  public visible  : boolean = false;
  public flat1    : boolean = false;

  Categories    : any[] = [];
  SubCategories : any   = [];
  finalc        : any[] = [];

  constructor(
    private _Categories : CategoriesService,
    private route       : ActivatedRoute,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
   
      this.name = params['name'];
      let sublevels = false;

      this._Categories.getCategories().pipe(first())
      .subscribe(
        data => {
          
          this.Categories = data.categories; 

          this.Categories.filter(datos =>
          {       
            
            if(datos.name == this.name){        
              if(datos.sublevels){
                this.finalc       = datos.sublevels;                     
                this.visible      = true; 
                this.flat1        = true;
                this.SubCategories.id = 0;
                sublevels         = true;
              }else{
                this.visible      = false;
              }
            }

            if(this.flat1 == false){
              this.finalc = [];
            }
            
          });
          
          this.flat1 = false;

          if(this.finalc.length == 0){

            this.findCategory(this.Categories, this.name);

            if(this.SubCategories){
              if(this.SubCategories.sublevels){
                this.finalc       = this.SubCategories.sublevels;
                this.visible      = true;
              }else{
                this.visible      = false;
              }
                          
            }           
            
          }

          if(this.SubCategories.sublevels){
            sublevels = true;
          }
          
          this.Products.publicproducts(this.SubCategories.id, sublevels);
          
      });    
    
    });
  }  

  findCategory(datos: any[], name: string, n = 0){

    datos.forEach(dato => {
      
      const index = dato.name.indexOf(name);
      
      if (index >= 0) {
        
        this.SubCategories  = dato;

        return true;

      }else{

        if(dato.sublevels){

          this.findCategory(dato.sublevels, name, n++);

        }

      }

    });    

  }

}
