import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { CategoriesComponent } from './categories/categories.component';
import { ProductsComponent } from './products/products.component'
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { OrderModule } from 'ngx-order-pipe';

/** Services */
import { ProductsService } from '../core/services/products.service';
import { WebSqlService } from '../core/services/websql.service';

@NgModule({
  declarations: [
    HomeComponent,
    CategoriesComponent,
    ProductsComponent,
    ShoppingCartComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    OrderModule
  ],
  providers: [
    ProductsService,
    WebSqlService
  ],
  bootstrap: []
})

export class PageModule { }
