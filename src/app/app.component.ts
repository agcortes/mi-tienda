import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { EncrypService } from './core/services/encryp.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor(
    private router: Router,
    private _cryp: EncrypService,
    
  ) { 

    //var decrypted = this._cryp.get('123456$#@$^@1ERF', encrypted);
    
    const dato = localStorage.key(0);
    
    if(dato == null){
    
      const encrypted = this._cryp.set('123456$#@$^@1ERF', 'pruebarappi');      
      localStorage.setItem('currentUser', JSON.stringify(encrypted));
    
    }
    
  }

}
