import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AdminLayoutComponent } from './@theme/layouts/admin-layout/admin-layout.component';
import { HomeComponent } from "../../src/app/page/home/home.component"

const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './@theme/layouts/admin-layout/admin-layout.module#AdminLayoutModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/',
  }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
})

export class AppRoutingModule {}
