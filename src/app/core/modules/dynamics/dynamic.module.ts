import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DynamicDirective } from '../../directives/dynamic.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    DynamicDirective,
  ],
  exports: [
    DynamicDirective,
  ],
  entryComponents: [
  ]
})

export class DynamicFormModule {}

