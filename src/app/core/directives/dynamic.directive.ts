import { ComponentFactoryResolver, ComponentRef, Directive, Input, OnChanges, OnInit, Type, ViewContainerRef } from '@angular/core';

import { Dynamic } from '../models/dynamic.interface';
import { DynamicConfig } from '../models/dynamic-config.interface';

const components: {[type: string]: Type<Dynamic>} = {
};

@Directive({
  selector: '[Dynamic]'
})

export class DynamicDirective implements Dynamic, OnChanges, OnInit {
  
  @Input()
  config: DynamicConfig;

  @Input()
  cols: boolean;
  
  @Input()
  submitted: boolean;

  @Input()
  colsn: number;

  component: ComponentRef<Dynamic>;

  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) {}

  ngOnChanges() {
        
    if (this.component) {
       
      this.component.instance.config    = this.config;
      this.component.instance.cols      = this.cols;
      this.component.instance.colsn     = this.colsn;
      this.component.instance.submitted = this.submitted;
    }

  }

  ngOnInit() {

    if (!components[this.config.type]) {
      const supportedTypes = Object.keys(components).join(', ');
      throw new Error(
        `Trying to use an unsupported type (${this.config.type}).
        Supported types: ${supportedTypes}`
      );
    }

    const component = this.resolver.resolveComponentFactory<Dynamic>(components[this.config.type]);
    this.component  = this.container.createComponent(component);

    this.component.instance.config    = this.config;
    this.component.instance.cols      = this.cols;
    this.component.instance.colsn     = this.colsn;
    this.component.instance.submitted = this.submitted;

  }

}
