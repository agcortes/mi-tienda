import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

/** Models */
import { Categories } from '../../core/models/categories.interface';

@Injectable({
  providedIn: 'root'
})

export class CategoriesService {

    constructor(private http: HttpClient) {}
        
    public getCategories(): Observable<Categories> {
        return this.http.get("assets/data/categories.json")        
        .pipe(
            map(categorias => {
                return (categorias as Categories);
            })
        );
    }

}