import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ProductsService {

    constructor(private http: HttpClient) {}
        
    public getProducts(): Observable<any> {
        return this.http.get("assets/data/products.json")        
        .pipe(
            map(products => {
                return products;
            })
        );
    }

}