import { Injectable } from '@angular/core';

// Permitimos que este objeto se pueda inyectar con la DI
@Injectable()

export class WebSqlService {

  private Conection : any;

  constructor() { 

    this.Conection = this.getConnection();
      
  }

  getConnection () {

    var shortName   = 'mytienda';
	  var version     = '1.0';
	  var displayName = 'My Tienda cache';
	  var maxSize     = 128535;

    return (<any>window).openDatabase(shortName, version, displayName, maxSize);

  }

  ReadData(params : Array<any>): Promise<any> {

    return new Promise<any>((resolve, reject) => {

      let data = [];
      let Query = "SELECT * FROM "+ params['table'] ;

      if(params['condition'].length > 0){
        Query = Query + " where " + params['condition'];
      }

      let createStatement = "CREATE TABLE  IF NOT EXISTS "+ params['table'] +" (id INTEGER PRIMARY KEY AUTOINCREMENT, id_product TEXT, quantity INTEGER, name TEXT, value INTEGER)";
      
      this.Conection.transaction(
        function(tx) {
          
          tx.executeSql(createStatement, []);

          tx.executeSql(Query, [],
            function (tx, results) {
              
              if (results.rows && results.rows.length) {                
                
                for (let prop of results.rows){
                    
                  if(prop.data){
                    prop.data = JSON.parse(prop.data);
                  }
                 
                  data.push(prop);
    
                }    
                
                resolve(data);

              }else{
                resolve(false);
              }

            },
            function (tx, error) {
              console.log(error);
            }
          );   
        
        }
      );   

    });

  } 

  InsertData(params : any): Promise<any> {

    return new Promise<any>((resolve, reject) => {

      let insertData = "INSERT INTO "+ params.table +" ("+ params.campos +") VALUES (?,?,?,?)";
      
      this.Conection.transaction(
        function(tx) {

          tx.executeSql(insertData, [params.value[0], params.value[1], params.value[2], params.value[3]]);

        }
      );

    });
  
  }

  UpdateData(params : any): Promise<any> {

    return new Promise<any>((resolve, reject) => {

      let UpdateData = "UPDATE "+ params.table +" SET "+ params.campos +" = ("+ params.campos +" + "+ params.value[0] +")  WHERE "+ params.condition;
      
      this.Conection.transaction(
        function(tx) {

          tx.executeSql(UpdateData, []);

        }
      );

    });
  
  }

  DeleteData(params : any): Promise<any> {

    return new Promise<any>((resolve, reject) => {

      let Query = "DELETE FROM "+ params.table ;

      if(params.condition.length > 0){
        Query = Query + " where " + params.condition;
      }

      this.Conection.transaction(
        function(tx) {
          tx.executeSql(Query, [],
            function (tx, results) {
              resolve(true);
            }
          );
        }
      );

    });
  
  }

  QueryData(query, list = true): Promise<any> {

    return new Promise<any>((resolve, reject) => {
      
      let data = [];

      this.Conection.transaction(
        function(tx) {
          tx.executeSql(query, [],
            function (tx, results) {
              
              if (results.rows && results.rows.length) {                
                
                for (let prop of results.rows){                  

                  if(list == true){

                    if(prop.data){
                      prop.data = JSON.parse(prop.data);
                    }
                   
                    data.push(prop);
                    
                  }else{
                    data = prop;
                  }
                  
                } 

                resolve(data);

              }else{
                resolve(false);
              }

            },
            function (tx, error) {
              console.log(error);
            }
          );
        }
      );

    });
  
  }

}
