import { ValidatorFn } from '@angular/forms';

export interface DynamicConfig {
  disabled?: boolean,
  Label?: string,
  Field: string,
  options?: any,
  placeholder?: string,
  type: string,
  validation?: ValidatorFn[],
  value?: any,
  DataType?: string,
  texts?: string,
  values?: string,
  event?: string  
}
