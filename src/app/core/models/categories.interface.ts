export interface Categories{
  categories : Array<
    {
      id?: number,  
      name?: string[],
      sublevels?: Array<any>
    }
  >
}