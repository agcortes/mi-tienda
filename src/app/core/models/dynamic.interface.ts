import { DynamicConfig } from './dynamic-config.interface';

export interface Dynamic {
  config    : DynamicConfig,
  cols      : boolean,
  colsn     : number,
  submitted : boolean,
}
