import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { EncrypService } from './core/services/encryp.service';

import { AdminLayoutComponent } from './@theme/layouts/admin-layout/admin-layout.component'; 
import { ComponentsModule } from './@theme/components/components.module';

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ComponentsModule    
  ],
  providers: [ EncrypService ],
  bootstrap: [AppComponent]
})

export class AppModule {}
