import { Component, OnInit, ElementRef, DoCheck } from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';

/** Services */
import { WebSqlService } from '../../../core/services/websql.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements DoCheck {

  private listTitles      : any[];
  private toggleButton    : any;
  private sidebarVisible  : boolean;
  location                : Location;
  mobile_menu_visible     : any = 0;
  total                   : number  = 0; 
  newtotal                : number  = 0; 
    
  public isCollapsed = true;

  constructor(
    location: Location,
    private element: ElementRef,
    private router: Router,
    private _WebSql: WebSqlService
  ){
    this.location = location;
    this.sidebarVisible = false;
  }

  ngOnInit(){

    let query = "SELECT count(id) AS total FROM products"
    this._WebSql.QueryData(query, false).then((response) => {
      this.total = response.total;
    });

  }

  ngDoCheck(){

    let query = "SELECT count(id) AS total FROM products"
    this._WebSql.QueryData(query, false).then((response) => {
      this.newtotal = response.total;
    });

    if(this.newtotal != this.total){
      this.total = this.newtotal; 
    }
    
  }

  collapse(){
      
    this.isCollapsed = !this.isCollapsed;
    const navbar = document.getElementsByTagName('nav')[0];
      
    if (!this.isCollapsed) {
      navbar.classList.remove('navbar-transparent');
      navbar.classList.add('bg-white');
    }else{
      navbar.classList.add('navbar-transparent');
      navbar.classList.remove('bg-white');
    }

  }

  sidebarOpen() {
      
    const toggleButton = this.toggleButton;
    const mainPanel =  <HTMLElement>document.getElementsByClassName('main-panel')[0];
    const html = document.getElementsByTagName('html')[0];
      
    if (window.innerWidth < 991) {
      mainPanel.style.position = 'fixed';
    }

    setTimeout(function(){
      toggleButton.classList.add('toggled');
    }, 500);

    html.classList.add('nav-open');

    this.sidebarVisible = true;

  }

}
