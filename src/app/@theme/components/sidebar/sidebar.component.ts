import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})

export class SidebarComponent implements OnInit {

  menuItems     : any     = [];
  expandedKeys  : any[]   = [];
  name          : string  = '';

  constructor(
    private _menu   : MenuService,    
    private route   : ActivatedRoute,
  ) { }

  ngOnInit() { 

    this.route.params.subscribe(params => {
      
      this.name = params['name'];
      this.expandedKeys.push('Bebidas');
    
    });
      
    this._menu.getMenu().subscribe(data => {
      this.menuItems = data.categories;
    });
   
  }

  isMobileMenu() {

    if ( window.innerWidth > 991) {
      return false;
    }
    return true;
  };


}
