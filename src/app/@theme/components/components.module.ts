import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent  } from './sidebar/sidebar.component';

import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { BannerComponent } from './banner/banner.component';

import { MenuService } from '../services/menu.service';
import { WebSqlService } from '../../core/services/websql.service'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    TreeViewModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    BannerComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    BannerComponent
  ],
  providers: [
    MenuService,
    WebSqlService
  ],  
})

export class ComponentsModule { }
