import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from "../../../page/home/home.component"
import { CategoriesComponent } from "../../../page/categories/categories.component"
import { ShoppingCartComponent  } from "../../../page/shopping-cart/shopping-cart.component"

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'categories/:name',
    component: CategoriesComponent,
  },
  {
    path: 'cart',
    component: ShoppingCartComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class AdminLayoutRoutingModule {}
