export class Menu {

  path: string;
  title: string;
  icon: string;
  class: string;
  type: string;

}
