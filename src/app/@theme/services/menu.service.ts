import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()

export class MenuService {

    constructor(private http: HttpClient) {}
    
    public getMenu(): Observable<any> {
        return this.http.get("assets/data/categories.json");
    }

}
