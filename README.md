# MyTienda

Este proyecto fue generedo con [Angular CLI](https://github.com/angular/angular-cli) version 7.2.3.

## Instalar 

Antes de instalar el proyecto se debe asegurar que este instalado node.js en su equipo versión 8 > y git  

Para Iniciar se debe clonar el proyecto que se encuentra ubicdo en bicbukect con el siguientes comando: 
git clone https://agcortes@bitbucket.org/agcortes/mi-tienda.git

Luego se ingresa a la carpeta creada para instalar el proyecto  se debe correr el siguientes comando:
npm install

## Servidor de Desarrollo 

Correr `ng serve` para iniciar el servidor de desarrollo. Navigate to `http://localhost:4200/`. 

## Ambiente Producción

Correr `ng build` Para crear el ambiente de producción. La compilación se almacenarán en el directorio `dist /`. Use la bandera `--prod` para una construcción de producción.

Se Utilizo como simulación de Back End el web sql de crhome, para guarda la sesión y los productos seleccionados para la compra.   